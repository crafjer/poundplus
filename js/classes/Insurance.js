
class Insurance {
    constructor(brand, year, type) {
        this.brand = brand;
        this.year = year;
        this.type = type;
        this.basePrice = 2000;
        this.totalPrice = 0;
    }

    quote() {
        /**
        *  1 = Americano = 1.15;
        *  2 = Asiatico = 1.05;
        *  3 = Europeo = 1.35;  
        */
        this.checkBrand();
    
        /**
         *  Si la fecha es menor a la fecha corriente se diminuirá 3% por cada año
        */
        this.checkYear();
    
        /**
         * Si el seguro es basico se incrementara en un 30%
         * Si el seguro es completo se incrementara en un 50%
         */
        this.checkType();
    }

    checkBrand() {
        switch(this.brand) {
            case 'americano':
                this.totalPrice = this.basePrice * 1.15;
            break;
            case 'asiatico':
                this.totalPrice = this.basePrice * 1.05;
            break;
            case 'europeo':
                this.totalPrice = this.basePrice * 1.35;
            break;
        }
    }

    checkYear() {
        let diffYear = new Date().getFullYear() - Number(this.year);
    
        this.totalPrice -= ((diffYear * 3) * this.totalPrice) / 100;
    }

    checkType() {
        if(this.type === 'basico') {
            this.totalPrice *= 1.3;
        } else {
            this.totalPrice *= 1.5;
        }
    }
    
}

export default Insurance;

