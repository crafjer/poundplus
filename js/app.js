//Variables
const form              = document.querySelector('#librajeForm');
const librajeList   = document.querySelector('.librajeList');
const  totalLibrajeContainer = document.querySelector('#totalLibrajeContainer') 
let librajes = [];

//EventListeners
init();


//functions
function init() {
    form.addEventListener('submit', addTweet);

    addEventListener('DOMContentLoaded', () => {
        librajes = JSON.parse( localStorage.getItem('librajes') );

        if( librajes === null) {
            librajes = [];
        } else {
            showlibrajes();
        }
    });
}

function addTweet(e) {
    e.preventDefault();
    
    let libraje = document.querySelector('#libraje').value;

    if(libraje === '') {
        return;
    }

    libraje = Number(libraje);

    if(isNaN(libraje)) {
        return;
    }

    const librajeObj = {
        id: Date.now(),
        name: libraje
    }

    librajes = [...librajes, librajeObj];

    saveTweet();

    form.reset();

    showlibrajes();
}

function saveTweet() {
    localStorage.setItem('librajes', JSON.stringify(librajes))
}

function deleteTweet(id) {
    librajes = librajes.filter(libraje => id !== libraje.id);
    
    saveTweet();

    showlibrajes();
    
}


function showlibrajes() {
    let totalLibraje = 0;

    if( librajes.length > 0 ) {
        cleanlibrajesContainer();
        librajes.forEach(libraje => {
            const a = document.createElement('a');
            a.className = 'borrar-tweet text-white';
            a.href = "#";
            a.textContent = 'X';
            a.onclick = () => {
                deleteTweet(libraje.id);
            }

            const span = document.createElement('span');
            span.className  = 'badge bg-danger rounded-pill';
            span.appendChild(a);

            const div1 = document.createElement('div');
            div1.className ='ms-2 me-auto';

            const div2 = document.createElement('div');
            div2.className = 'fw-bold';
            div2.textContent = libraje.name;

            div1.appendChild(div2);

            const li = document.createElement('li');
            li.className = 'list-group-item d-flex justify-content-between align-items-start';

            li.appendChild(div1);
            li.appendChild(span);

            librajeList.appendChild(li)

            //total Libraje
            totalLibraje += libraje.name;
        });
    } else {
        cleanlibrajesContainer();
        totalLibrajeContainer.innerHTML = '';
    }

    if(totalLibraje > 0) {
        totalLibrajeContainer.innerHTML = `<strong>Total Libraje</strong>: ${totalLibraje}`;
    }

}

function cleanlibrajesContainer() {
    while(librajeList.firstElementChild) {
        librajeList.removeChild(librajeList.firstElementChild);
    }
}